# Nuxt Portfolio Site

Version 1.0 of Portfolio site integrated with Nuxt.js for Server-side rendering capabilities.

Site URL: https://thefinolword.com

Repository of old Vue.js non-SSR version of site: https://gitlab.com/omarfevans/portfolio-site