import Vue from 'vue';
import VueScrollReveal from 'vue-scroll-reveal';

// OR specifying custom default options for all uses of the directive
Vue.use(VueScrollReveal, {
    class: 'v-scroll-reveal',
    duration: 2000,
    origin: 'top',
    distance: '100px',
    viewFactor: 0.2,
    mobile: false
});

// , {
//     class: 'v-scroll-up',
//     duration: 2000,
//     origin: 'bottom',
//     distance: '100px',
//     viewFactor: 0.2,
//     mobile: false
// }

// window.sr.reveal('.section-heading', {
        //     duration: 2000,
        //     origin: 'top',
        //     distance: '50px',
        //     viewFactor: 0.2,
        //     mobile: false
        // })

        // window.sr.reveal('.profile-picture-column', {
        //     duration: 2000,
        //     origin: 'left',
        //     distance: '200px',
        //     viewFactor: 0.2,
        //     mobile: false
        // })
        // window.sr.reveal('.description', {
        //     duration: 2000,
        //     origin: 'right',
        //     distance: '200px',
        //     viewFactor: 0.2,
        //     mobile: false
        // })

        // window.sr.reveal('.front-end', {
        //     duration: 2000,
        //     origin: 'left',
        //     distance: '100px',
        //     viewFactor: 0.2,
        //     mobile: false
        // })
        // window.sr.reveal('.back-end', {
            // duration: 2000,
            // origin: 'bottom',
            // distance: '100px',
            // viewFactor: 0.2,
            // mobile: false
        // })
        // window.sr.reveal('.cms', {
        //     duration: 2000,
        //     origin: 'right',
        //     distance: '100px',
        //     viewFactor: 0.2,
        //     mobile: false
        // })