const ExperienceList = [{
    title: 'Speed Score App',
    featured_image: '/experience/speedscore.png',
    tools: ['/logos/react.svg','/logos/mongodb.svg','/logos/express.svg','/logos/aws.svg'],
    type: 'site',
    url: `http://speedgolfapp-env.eba-feirnjig.us-east-1.elasticbeanstalk.com/`,
    description: `<b>Please Read: If you wish to demo this application, do not need to put in <i>any</i> personal information to create an account. If you wish to try out my app, but do not want to create an acccount, just use the following credentials for a public user: (Username: omar, Password: celery)</b><br /><br />SpeedScore is an application that was the main assignment for my Web Development course at Washington State University. The project was assigned by my professor, and the company logo and name was made up by him, but the designs are all mine, including the golf swinger in the background.<br />I used React for the front end and Express to write the API routes for storing fetching golfer data. I used Passport to authenticate users both locally and with third parties like Github and Facebook. Deployed on Elastic Beanstalk.`,
    github: 'https://github.com/omar-enrique/Speed-Golf-App'
},
{
    title: 'Portfolio Site',
    featured_image: '/experience/portfolio.png',
    tools: ['/logos/vue.svg','/logos/mongodb.svg','/logos/nodejs.svg','/logos/strapi.svg'],
    description: `Through the development of my portfolio site, I took the opportunity to explore Vue.js, and its server-side relative, Nuxt.js. Vue/Nuxt is a truly underrated Javascript framework. It's scalable, with thorough documentation, and adopts a Javascript/CSS/HTML component-based development model similar to Ionic/Angular, which I really like.<br /><br />I was very excited to find Strapi during my research phase of building my website. Strapi is an open source headless CMS built with Node.js, which has a beautiful interface for connecting with your MongoDB. I use Strapi to add new articles to my MongoDB, and create GraphQL endpoints to fetch those articles.`,
    type: 'portfolio',
    github: 'https://gitlab.com/omarfevans/nuxt-portfolio-site'
},
{
    title: 'Under Construction: Insurance 4 Idaho Website',
    featured_image: '/experience/insurance4idaho.png',
    tools: ['/logos/react.svg','/logos/nextjs.svg','/logos/bootstrap.svg','/logos/nodejs.svg'],
    type: 'site',
    description: `<b>Disclaimer: I am currently still building Insurance 4 Idaho's new website, but as it is still under construction, I do not have a demo available. Only sample photos for now.</b><br /><br />Insurance 4 Idaho is an independent insurance agency located in Boise, Idaho. I am currently helping them rebuild their website from the ground up with modern SEO practices, and with cheaper website hosting rates.  I am using Next.js to be able to render the website on the server-side so I can better add meta tags to each web page and make the website more SEO compatible.`
},
{
    title: `Meter Group's Skala Mobile App`,
    featured_image: '/experience/skala-mobile.jpg',
    tools: ['/logos/ionic.svg','/logos/django.svg','/logos/nodejs.svg','/logos/sql.svg'],
    description: `Meter Group delivers products and services relating to the data processing and analytics for Food Manufacturers. In internship with Meter Group, I assisted in the Full-Stack development of Skala Mobile, an iPad application designed to allow customers to interface with data on their food products, as well as their devices for measuring water activity.<br /><br />Client side is built with Ionic Framework, with Bootstrap integrated for frontend components. Used Django for backend scripting and content management.`,
    type: 'app',
    url: `https://apps.apple.com/us/app/skala-mobile-by-meter/id1325783480`
}]

export default ExperienceList;